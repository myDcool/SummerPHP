<?php

/**
验证类，验证传入参数的合法性
**/
class VerifyConfig
{   
    public static function getConfig()
    {
        return [
            'uri' => [
                'uri' => [
                    'rule' => 'anyOf',
                    'ruler' => [Verify::rule_empty,'/^[a-zA-Z0-9_&=\-]+$/'],
                    'msg' => '请求的链接只能包含: 英文字母，数字，下划线，&， =， -'
                ]
            ],

            'user:login:index' => [
                'username' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::rule_alpha, Verify::rule_number, Verify::getRuleLengthRange(5, 15)],
                    'msg' => '用户名需要是5-15个字符, 包含字母和数字'],
                'password' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::rule_alpha, Verify::rule_number, Verify::getRuleLengthRange(10, 20)],
                    'msg' => '密码需要是10-20个字符, 包含字母和数字'],
            ],

            'user:register:index' => [
                'username' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::rule_alpha, Verify::rule_number, Verify::getRuleLengthRange(5, 15)],
                    'msg' => '用户名需要是5-15个字符, 包含字母和数字'],
                'mobile' => [
                    'rule' => 'anyOf',
                    'ruler' => [Verify::getRuleMobile(), Verify::rule_empty],
                    'msg' => '手机号格式不正确'],
                'password' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::rule_alpha, Verify::rule_number, Verify::getRuleLengthRange(10, 20)],
                    'msg' => '密码需要是10-20个字符, 包含字母和数字'],
                'password_confirm' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::rule_alpha, Verify::rule_number, Verify::getRuleLengthRange(10, 20)],
                    'msg' => '密码需要是10-20个字符, 包含字母和数字'],
            ],

            'admin:article:addArticleType' => [
                'title' => [
                    'rule' => 'allOf',
                    'ruler' => [Verify::getRuleLengthRange(2, 15),'/^[a-zA-Z0-9_\x{4e00}-\x{9fa5}]+$/u'],
                    'msg' => '标题需要是长度为2-10个字符，并由字母、数字、汉字组成'
                ]
            ]

        ];
    }

}

