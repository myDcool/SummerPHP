<?php

class RouteConfig
{
    public static $defaultModule = 'index'; //默认模块
    public static $defaultCtrl = 'index'; //默认控制器
    public static $defaultAction = 'index'; //默认方法
    public static $Path = array(
        
        'user' => 'user/index',
        'user_login' => 'user/login',
        'user_islogin' => 'user/login/iisLogin',
        'user_register' => 'user/register',
        'user_register_vcode' => 'user/index/vcode',
        'user_logout' => 'user/logout',
        'user_name_repeat' => 'user/register/isUsernameRepeat',

        //Redis队列
        'queue_test' => 'cli/queue/test', //测试生产消息
        'queue_brpop_all' => 'cli/queue/blockpopAll', //一次性阻塞出所有队列
        'queue_blockpop_([a-zA-Z0-9_]+)' => 'cli/queue/blockpop/queueKey/$1', //阻塞出单个队列
        'queue_restart_([a-zA-Z0-9_]+)' => 'cli/queue/restart/queueKey/$1', //重启出队列
        'queue_kill_([a-zA-Z0-9_]+)' => 'cli/queue/kill/queueKey/$1', //停掉出队列

        //db队列
        'cli_queue_db_consumer' => 'cli/queue_db/consumer', //队列消费程序

        'interface_consumer' => 'interface/consumer/handle', //异步消息接收处理

        //接口相关
        'interface_record_test' => 'interface/record/test',
        'interface_record_add' => 'interface/record/add',
        'interface_record_modify' => 'interface/record/modify',
        'interface_record_delete' => 'interface/record/delete',
        
        'v_.*' => 'index/index/index', //可用于单页面路由
        'db_hosts_name' => 'index/index/dbHostsName',
        'db_databases' => 'index/index/dbDatabase',
        'db_tables' => 'index/index/dbTables',
        'db_fields' => 'index/index/dbFields',

        'article_list_(\d+)' => 'index/index/route/page/$1',
        'test_code' => 'index/index/uuCode',
        'test_sql' => 'index/index/sql',
        'test_request' => 'index/index/req',

    );
}