<?php

class Lang
{
    const db_connect_create = '创建数据库链接 ';
    const db_connect_error = '数据库链接失败 ';
    const db_database_not_exist = '数据库不存在 ';
    const db_table_not_exist = '表不存在 ';
    const db_query_field_not_exist = '字段不存在 ';
    const db_query_empty = '执行SQL为空';
    const db_no_where = '没有指定where条件';
    const db_where_in_empty = 'where in 条件为空';
    const db_value_not_number = '值不是数字';
    const db_value_not_support = '值既不是数字, 也不是字符串 ';

}