<?php

class RedisConfig
{
    public static $hosts = [
        'localhost' => ['127.0.0.1', 6379, ''],
    ];

    //topic名字列表(也就是队列名字列表)
    const topic_test = 'test_queue';
    const topic_record_add = 'record_add';
    const topic_record_modify = 'record_modify';
    const topic_record_delete = 'record_delete';

    //每个topic需要通知的消费者URL
    public static $notifyUrl = [

        //通用, 添加数据消息
        self::topic_test => [
            INTERFACE_URL_QUEUE_DB.'interface_test'
        ],

        //通用, 添加数据消息
        self::topic_record_add => [
            INTERFACE_URL_QUEUE_DB.'interface_record_add'
        ],

        //通用, 修改数据消息
        self::topic_record_modify => [
            INTERFACE_URL_QUEUE_DB.'interface_record_modify'
        ],

        //通用, 删除数据消息
        self::topic_record_delete => [
            INTERFACE_URL_QUEUE_DB.'interface_record_delete'
        ],

    ];

    //获取某个topic消息需要通知的URL
    public static function getTopicNotifyUrl($topic)
    {
        if (!empty(self::$notifyUrl[$topic])) {
            return self::$notifyUrl[$topic];
        } else {
            return [];
        }
    }

}