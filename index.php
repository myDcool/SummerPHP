<?php
//需要 php 5.6+
date_default_timezone_set('Asia/Shanghai');
mb_internal_encoding("UTF-8");
session_start();

//error_reporting(0); //取消注释后, 报错信息会在返回到前端

//检测服务器负载
if (function_exists('sys_getloadavg')) {
    $load = sys_getloadavg();
    if ($load[0] > 90) {
        header('HTTP/1.1 503 Too busy, try again later');
        die('服务器忙, 请稍后再试.');
    }
}

define('ENV', 'pro');
define('PROJECT_ID', 'xxx'); //用来生成全局数字id
define('PROJECT_NAME', 'xxx'); //用来记录用户的来源
define('LOG_LEVEL', 2); //1:错误, 2:警告, 3:提醒, 4:调试

//要在这里宏定义ROOT因为本文件是入口文件,所有的include/require相对路径时都是以此文件所在目录为基准目录
$root = str_replace('\\', '/', __DIR__).'/';
define('ROOT',          $root);
define('COREPATH', 		ROOT.'core/'); //框架核心目录
define('LIBPATH', 		ROOT.'libs/'); //库文件目录
define('MODULEPATH', 	ROOT.'modules/'); //模块目录
define('MODELPATH', 	ROOT.'model/'); //model目录
define('VIEWPATH', 	    ROOT.'view/'); //视图目录
define('CONFIGPATH', 	ROOT.'config/'); //配置文件目录
define('STATICPATH', 	ROOT.'static/'); //静态文件目录: js/css/image
define('LOGPATH', 	    ROOT.'log/'); //日志文件目录

//下边参数的定义是以nginx为webserver获取的
$scheme = empty($_SERVER['REQUEST_SCHEME']) ? 'http' : $_SERVER['REQUEST_SCHEME'];
define('HTTP_HOST',		$_SERVER['HTTP_HOST']); //http_host没有包含端口
define('BASEURL',		$scheme.'://'.HTTP_HOST.'/');
define('REQUEST_URI',	$_SERVER['REQUEST_URI']); //包含了?后的get参数, 也包含问号前的斜线
define('HTTP_REFERER',	empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER']);
define('DOCUMENT_URI',	$_SERVER['DOCUMENT_URI']);
define('REQUEST_TIME', 	$_SERVER['REQUEST_TIME']);
define('REQUEST_TIME_FLOAT',$_SERVER['REQUEST_TIME_FLOAT']);
define('REQUEST_DATETIME', date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']));
define('REQUEST_DATE', date('Y-m-d', $_SERVER['REQUEST_TIME']));
define('REQUEST_DAY', date('Y-m-d', $_SERVER['REQUEST_TIME']));
define('SEPARTOR', '__'); //全局分隔符
define('SECOND_HOUR', 3600);
define('SECOND_DAY', 86400);
define('IS_POST', ($_SERVER['REQUEST_METHOD'] == 'POST'));

define('VIEW_FLODER_NAME', 		'view'); //视图目录的名字
define('PHP_FILE_EXTENSION',	'.php'); //PHP文件的后缀, 也可以是.class.php
define('TPL_FILE_EXTENSION',	'.html'); //模版文件的后缀, 也可以是.php结尾

define('UNIQID', uniqid()); //PHP基于微秒的唯一值(字母和数字的组合16位), 可用于文件日志的跟踪编号, 便于筛选,跟踪
define('MYSQL_HOST_FILE', '/xx/xx/dbConfig.json'); //存放mysql主机信息的json文件地址, 参考 tool/dbConfig.json
define('INTERFACE_URL_QUEUE_DB', 'https://xx.xx.xx/'); //处理db队列消息的接口地址(或网关)

require_once(COREPATH.'Main.php');//读取核心控制器基类文件
require_once(COREPATH.'Load.php'); //自动加载类

//自动加载函数
spl_autoload_register(array('Load', 'Core'));	//核心类
spl_autoload_register(array('Load', 'Lib'));	//第三方类库
spl_autoload_register(array('Load', 'Config'));	//配置文件1, 区分运行环境env (优先)
spl_autoload_register(array('Load', 'PublicConfig'));	//配置文件2, 不区分env的配置文件
spl_autoload_register(array('Load', 'Model'));	//模型类
spl_autoload_register(array('Load', 'BaseCtrl')); //自定义基类控制器

//捕获异常. final
set_exception_handler('exception_handler');
function exception_handler($e) {
    FileLog::error($e->getTraceAsString(), 'exception');
    Response::error($e->getMessage());
}

//捕获非异常的服务器错误. final
register_shutdown_function('shutdown');
function shutdown() {
    $bitmask = E_ERROR | E_WARNING; //记录这几种错误
    $last_error = error_get_last();
    if(!empty($last_error) && ($last_error['type'] & $bitmask) > 0) {
        FileLog::error($last_error, 'error_shutdown');
        //Response::error('服务器错误');
    }
}

//header('Access-Control-Allow-Origin: '.BASEURL); //该字段是必须, 它的值要么是一个域名值(要带上http或https)，要么是一个*，表示接受任意域名的请求
//header('Access-Control-Allow-Credentials: true'); //该字段可选, 它的值要么是true, 要么没有这条语句; 表示是否允许发送Cookie, true: 请求时带上cookie, 设置为true时, Access-Control-Allow-Origin的值不能为*
//header('Access-Control-Request-Method: GET,POST'); //该字段是必须的

header("Content-type: text/html; charset=utf-8"); //后边如果再有 header content-type, 就会覆盖本句中的设置

$GLOBALS['DB_LINKS'] = array(); //存储所有连接, 任意地方使用: $DB_LINKS

Main::_run(); //在Main类中动态加载其它controller类
exit;