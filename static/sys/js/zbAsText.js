const zbAsText = function(){
    this.onConfirm = null; //点击确认后执行回调
    this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击

    //浮层模板
    this.tpl = '<div id="zbAsText" class="zb-flex-col-bottom zb-flex-nowrap zb-width-r100">\
        <div id="zbASMask" class="zb-flex-grow1"></div>\
        <div id="zbASBox" class="zb-flex-col-between zb-flex-stretch">\
          <div id="zbASTitle" class="zb-text-center"></div>\
          <div id="zbASBody"></div>\
          <div id="zbASFooter" class="zb-flex-row-around zb-flex-center">\
           <div class="zbASBtn" id="zbASBtnCancel">取消</div>\
           <div class="zbASBtn" id="zbASBtnConfirm">确认</div>\
          </div>\
         </div>\
         <input type="hidden" id="zbInput" value="">\
        </div>';

    //初始化
    this.init = function (title='') {
        let flag = document.getElementById('zbAsText');
        if (flag == null) {
            //初始化css
            this.initCss();

            //初始模板, 绑定事件
            let body = document.getElementsByTagName('body')[0];
            let node = this.htmlToNode(this.tpl);
            node.querySelector('#zbASMask').addEventListener('click', this.hide.bind(this));
            node.querySelector('#zbASBtnCancel').addEventListener('click', this.hide.bind(this));
            node.querySelector('#zbASBtnConfirm').addEventListener('click', this.confirm.bind(this));
            body.appendChild(node);
        }

        document.getElementById('zbASTitle').innerText = title;
    }

    this.htmlToNode = function (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    this.hide = function () {
        document.getElementById('zbAsText').style.height = '0';
        document.getElementById('zbASBody').innerHTML = '';
        this.isConfirm = 0;
    }

    //显示弹出层
    this.show = function () {
        document.getElementById('zbAsText').style.height = '100%';
    }

    //点击确认按钮触发执行
    this.confirm = function () {
        if (this.isConfirm === 1) {
            console.log('重复点击');
            return;
        }
        this.isConfirm = 1;
        let input = document.getElementById('zbInput');
        let obj = this.decodeObj(input.value);
        input.value = ''; //清空已存储的数据
        this.hide();
        if (typeof this.onConfirm === 'function') {
            this.onConfirm(obj);
        }
    }

    //清除所有的class
    this.clearClass = function(id) {
        let obj = document.getElementById(id);

        let cls = [];
        for (let i=0; i<obj.classList.length; i++) {
            cls.push(obj.classList[i]);
        }

        for (let i=0; i<cls.length; i++) {
            obj.classList.remove(cls[i]);
        }
    }

    //生成样式
    this.initCss = function() {
        let style = document.createElement('style');
        style.innerText =
            '.zb-flex-col-top {display:flex;flex-direction:column;justify-content:flex-start}'+
            '.zb-flex-col-bottom {display:flex;flex-direction:column;justify-content:flex-end}'+
            '.zb-flex-col-between {display:flex;flex-direction:column;justify-content:space-between}'+
            '.zb-flex-col-center {display:flex;flex-direction:column;justify-content:center}'+
            '.zb-flex-col-around {display:flex;flex-direction:column;justify-content:space-around}'+
            '.zb-flex-row-center {display:flex;flex-direction:row;justify-content:center}'+
            '.zb-flex-row-around {display:flex;flex-direction:row;justify-content:space-around}'+
            '.zb-flex-row-between {display:flex;flex-direction:row;justify-content:space-between}'+
            '.zb-flex-row-right {display:flex;flex-direction:row;justify-content:flex-end}'+
            '.zb-flex-stretch {align-items:stretch}'+
            '.zb-flex-center {align-items:center}'+
            '.zb-flex-grow1 {flex-grow:1}'+
            '.zb-flex-nowrap {flex-wrap:nowrap}'+
            '.zb-text-center {text-align:center}'+
            '.zb-scroll-x {overflow-x:scroll; white-space: nowrap;}'+
            '.zb-scroll-x::-webkit-scrollbar {display:none}'+
            '.zb-scroll-y {overflow-y:scroll; white-space: nowrap;}'+
            '.zb-scroll-y::-webkit-scrollbar {display:none}'+
            '.zb-scroll{overflow:scroll; white-space: nowrap;}'+
            '.zb-scroll::-webkit-scrollbar {display:none}'+
            '.zb-width-r100{width:100%;}'+
            '.zb-height-r100{height:100%;}'+
            '#zbAsText{position:fixed;top:0;height:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4);}'+
            '#zbAsText #zbASMask{}'+
            '#zbAsText #zbASBox{max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsText #zbASTitle{height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee;}'+
            '#zbAsText #zbASBody{min-height:100px;}'+
            '#zbAsText #zbASFooter{padding:5px;border-top:1px solid #eeeeee;height:40px;line-height:40px;}'+
            '#zbAsText .zbASBtn{width:50%;font-size:20px;text-align:center;}'+
            '#zbAsText .zb-input{ height:30px;}'+
            '#zbAsText .zb-text{width:100%; max-width:100%; height:100%; min-height:100px; word-wrap:break-word;}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    this.replaceNode = function (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    //添加文字信息, 文本，日期，数字
    this.addInput = function (config) {
        this.clearClass('zbASBody');
        let box = document.getElementById('zbASBody');
        box.classList.add('zb-flex-col-center');
        box.classList.add('zb-flex-stretch');

        //初始化input标签
        let attrId = 'zb-input'
        let input = document.createElement('input');
        input.setAttribute('id', attrId);
        for (let k in config['attr']) {
            if (k === 'id') {continue}
            input.setAttribute(k, config['attr'][k]);
        }

        input.setAttribute('data-params', this.encodeObj(config['params']));
        input.addEventListener('input', this.changeTextParams.bind(this, attrId))
        input.classList.add('zb-input');
        box.appendChild(input);

        //初始化计数器
        let maxLength = config['attr']['maxlength'] ? config['attr']['maxlength'] : 0;
        if (maxLength > 0) {
            let defaultLength = config['attr']['value'] ? config['attr']['value']['length'] : 0;
            let tpl = '<div class="zb-flex-row-right"><div id="zb-text-length">'+defaultLength+'</div>/<div>'+maxLength+'</div></div>';
            let node = this.htmlToNode(tpl);
            box.appendChild(node);
        }
    }

    //添加文字信息, 可换行
    this.addTextarea = function (config) {
        this.clearClass('zbASBody');
        let box = document.getElementById('zbASBody');
        box.style.height = '500px';
        box.classList.add('zb-flex-col-center');
        box.classList.add('zb-flex-stretch');

        //初始化textarea标签
        let attrId = 'zb-textarea';
        let input = document.createElement('textarea');
        input.setAttribute('id', attrId);
        for (let k in config['attr']) {
            if (k === 'id') {continue};
            input.setAttribute(k, config['attr'][k]);
        }
        input.setAttribute('data-params', this.encodeObj(config['params']));
        input.addEventListener('input', this.changeTextParams.bind(this, attrId))
        input.setAttribute('style', 'height:450px')
        input.innerHTML = (config['value']) ? config['value'] : '';
        box.appendChild(input);

        //初始化计数器
        let maxLength = config['attr']['maxlength'] ? config['attr']['maxlength'] : 0;
        if (maxLength > 0) {
            let defaultLength = config['value'] ? config['value']['length'] : 0;
            let tpl = '<div class="zb-flex-row-right"><div id="zb-text-length">'+defaultLength+'</div>/<div>'+maxLength+'</div></div>';
            let node = this.htmlToNode(tpl);
            box.appendChild(node);
        }
    }

    //输入文本框时, 记录下输入的值, 并实时计算字数
    this.changeTextParams = function (id){
        let obj = document.getElementById(id);
        //变更长度
        let maxLength = obj.getAttribute('maxlength');
        if (maxLength) {
            let currLen = obj.value.length
            document.getElementById('zb-text-length').innerText = (currLen > maxLength) ? maxLength : currLen;
            if (currLen > maxLength) {
                obj.value = obj.value.substring(0, maxLength);
            }
        }

        //记录下输入的值
        let params = obj.getAttribute('data-params');
        let p = this.decodeObj(params);
        p['value'] = obj.value;
        document.getElementById('zbInput').value = this.encodeObj(p);
    }

    //是否是数组
    this.isArray = function (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    this.error = function(str) {
        console.log(str);
    }

    this.encodeObj = function (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    this.decodeObj = function (str) {
        return JSON.parse(decodeURIComponent(str));
    }
}
/**
 * 用法举例:
    let input = new zbAsText();
    let title = 'this is title';
    let params = {a:1, b:2}; //透传参数
    
    let confirm = function(params){
        let str = JSON.stringify(params);
        document.getElementById('selected').innerText = str;
    }

    //input标签的属性
    let config = {id:'test',  type: 'number', value: 0, maxlength: 5, step: 1,autocomplete:'off', autofocus:true};
    //let config = {id:'test',  type: 'number', value: 0, maxlength: 5, step: 0.1,autocomplete:'off',};
    //let config = { id:'test',  type: 'text', value: 'aaa', maxlength: 10, autocomplete:'off',};
    //let config = { id:'test',  type: 'date', value: '2023-01-01', maxlength: 10, autocomplete:'off',};
    //let config = { id:'test',  type: 'datetime-local', value: '2023-01-01T00:00:00', min: '1900-01-01T00:00:00', step:1,  autocomplete:'off',};
    //let config = { id:'test',  type: 'email', value: '', maxlength:100,  autocomplete:'off',};
    //let config = { id:'test',  type: 'textarea', value: '1111', maxlength:100,  autocomplete:'off',};


    //渲染组件
    text(title, config, params, confirm);

    function text(title, config, params, onConfirm) {
        input.init(title);
        input.onConfirm = onConfirm;
        
        if (config.type !== 'textarea') {
            input.addInput({attr:config, params:params});
            input.show();
        } else {
            value = config.value;
            delete config.value;
            input.addTextarea({attr:config, value:value, params:params});
            console.log(config);
            input.show();
        }
    }
 */