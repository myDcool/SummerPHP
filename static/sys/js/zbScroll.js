const zbScroll = function() {
    this.params = {}

    //滚动到顶部
    this.goScrollTop = function () {
        //已经滚动了多高
        let scrolledLen = (new zbScroll()).scrollTop();
        //把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
        //向上是负数，向下是正数
        window.scrollBy(0, -scrolledLen);
        return true;
    }

    //获取页面文档的总高度
    this.documentHeight = function (){
        //现代浏览器（IE9+和其他浏览器）和IE8的document.body.scrollHeight和document.documentElement.scrollHeight都可以
        return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight);
    }

    //获取窗口顶部已遮盖的的文档高度
    this.scrollTop = function () {
        let len = Math.max(
            //chrome
            document.body.scrollTop,
            //firefox/IE
            document.documentElement.scrollTop);

        return Math.round(len);
    }

    //获取窗口的高度
    this.windowHeight = function (){
        return (document.compatMode == "CSS1Compat")
            ? document.documentElement.clientHeight
            : document.body.clientHeight;
    }

    //是否已经滑倒了底部
    this.isScrollBottom = function (offset = 20) {
        let total_doc_len = this.documentHeight(); //文档总长度
        let already_scroll_len = this.scrollTop() + this.windowHeight(); //已经显示的长度
        let diff = total_doc_len - already_scroll_len;

        if( diff < offset) { //两者之差小于offset时， 说明已经快到底了
            return true;
        } else {
            return false;
        }
    }
}
