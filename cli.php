<?php
date_default_timezone_set('Asia/Shanghai');
mb_internal_encoding("UTF-8");
header("Content-type: text/html; charset=utf-8");

if (php_sapi_name() !== 'cli') {
	exit(404);
}

define('ENV', 'pro');
define('PROJECT_ID', 'xxx'); //用来生成全局数字id
define('PROJECT_NAME', 'xxx'); //用来记录用户的来源
define('LOG_LEVEL', 2); //1:错误, 2:警告, 3:提醒, 4:调试

//要在这里宏定义ROOT因为本文件是入口文件,所有的include/require相对路径时都是以此文件所在目录为基准目录
$root = str_replace('\\', '/', __DIR__).'/';
define('ROOT',          $root);
define('COREPATH', 		ROOT.'core/'); //框架核心目录
define('LIBPATH', 		ROOT.'libs/'); //库文件目录
define('MODULEPATH', 	ROOT.'modules/'); //模块目录
define('MODELPATH', 	ROOT.'model/'); //model目录
define('CONFIGPATH', 	ROOT.'config/'); //配置文件目录
define('STATICPATH', 	ROOT.'static/'); //静态文件目录: js/css/image
define('LOGPATH', 	    ROOT.'log/'); //日志文件目录

define('PHPCLI', 'php'); //PHP命令(最好是绝对路径)

$arr = getopt('q:'); //获取命令行参数
if (empty($arr['q'])) {
	exit('请输入参数 -q xxx'.PHP_EOL);
}

define('DOCUMENT_URI',	$arr['q']);

define('REQUEST_TIME', 	time());
define('REQUEST_TIME_FLOAT', microtime(true));
define('REQUEST_DATETIME', date('Y-m-d H:i:s'));
define('REQUEST_DATE', date('Y-m-d'));
define('REQUEST_DAY', date('Y-m-d'));
define('REQUEST_URI', ''); //
define('BASEURL', ''); //
define('SEPARTOR', '__'); //全局分隔符
define('SECOND_HOUR', 3600);
define('SECOND_DAY', 86400);
define('IS_POST', false);

define('VIEW_FLODER_NAME', 		'view'); //视图目录的名字
define('PHP_FILE_EXTENSION',	'.php'); //PHP文件的后缀, 也可以是.class.php
define('TPL_FILE_EXTENSION',	'.html'); //模版文件的后缀, 也可以是.php结尾

define('UNIQID', uniqid()); //PHP基于微秒的唯一值(字母和数字的组合16位), 可用于文件日志的跟踪编号, 便于筛选,跟踪
define('MYSQL_HOST_FILE', '/xx/xx/dbConfig.json'); //存放mysql主机信息的json文件地址, 参考 tool/dbConfig.json
define('INTERFACE_URL_QUEUE_DB', 'https://xx.xx.xx/'); //处理db队列消息的接口地址(或网关)

require_once(COREPATH.'Main.php');//读取核心控制器基类文件
require_once(COREPATH.'Load.php'); //自动加载类

//自动加载函数
spl_autoload_register(array('Load', 'Core'));	//核心类
spl_autoload_register(array('Load', 'Lib'));	//第三方类库
spl_autoload_register(array('Load', 'PublicConfig'));	//不区分env的配置文件
spl_autoload_register(array('Load', 'Config'));	//配置文件
spl_autoload_register(array('Load', 'Model'));	//模型类
spl_autoload_register(array('Load', 'BaseCtrl')); //自定义基类控制器

set_exception_handler('exception_handler');
function exception_handler($e) {
    FileLog::error($e->getTraceAsString(), 'exception');
    Response::error($e->getMessage());
}

register_shutdown_function('shutdown');
function shutdown() {
    $bitmask = E_ERROR | E_WARNING; //记录这几种错误
    $last_error = error_get_last();
    if(!empty($last_error) && ($last_error['type'] & $bitmask) > 0) {
        FileLog::error($last_error, 'error_shutdown');
        //Response::error('服务器错误');
    }
}

Main::_runcli(); //在Main类中动态加载其它controller类
exit;