CREATE DATABASE IF NOT EXISTS queue
--    DEFAULT CHARACTER SET utf8mb4
--    DEFAULT COLLATE utf8mb4_general_ci
;
use queue;
-- 消息体
CREATE TABLE `queue_message` (
     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
     `msg_code` varchar(20) NOT NULL DEFAULT '' COMMENT '消息唯一编码',
     `topic_name` varchar(50) NOT NULL DEFAULT '' COMMENT '消息话题',
     `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '状态, 0未处理; 1分发中; 2分发完毕',
     `message` varchar(1500) NOT NULL DEFAULT '' COMMENT '消息体',
     `tag` varchar(100) NOT NULL DEFAULT '' COMMENT '便于筛选的标志',
     `add_time` datetime NOT NULL DEFAULT current_timestamp(),
     `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),

     PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 ;

-- 消费者列表
CREATE TABLE `queue_consumer` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `topic_name` varchar(50) NOT NULL DEFAULT '' COMMENT '消息话题',
      `name` varchar(20) NOT NULL DEFAULT '' COMMENT '消费者编码',
      `interface` varchar(500) NOT NULL DEFAULT '' COMMENT '接收消息的接口',
      `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '状态,  0:生效; 1:失效',
      `add_time` datetime NOT NULL DEFAULT current_timestamp(),
      `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),

      PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 ;

-- 消息处理状态
CREATE TABLE `queue_handle` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(20) NOT NULL DEFAULT '' COMMENT '消费者编码',
    `msg_code` varchar(20) NOT NULL DEFAULT '' COMMENT '消息唯一编码',
    `topic_name` varchar(50) NOT NULL DEFAULT '' COMMENT '消息话题',
    `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '状态, -1:处理失败, 0未处理; 1处理中; 2处理完毕',
    `add_time` datetime NOT NULL DEFAULT current_timestamp(),
    `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),

    PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 ;

