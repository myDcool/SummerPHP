-- alter table 表名 default character set utf8 collate utf8_general_ci;
-- alter table `表名` convert to character set utf8;

CREATE DATABASE IF NOT EXISTS test
--    DEFAULT CHARACTER SET utf8mb4
--    DEFAULT COLLATE utf8mb4_general_ci
;

use test;

CREATE TABLE `user` (
    `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `unid` char(32) NOT NULL DEFAULT '',
    `status` tinyint(4) NOT NULL DEFAULT 0,
    `is_admin` tinyint(4) NOT NULL DEFAULT 0,
    `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
    `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
    `email` varchar(50) NOT NULL DEFAULT '',
    `password` varchar(64) NOT NULL DEFAULT '' COMMENT '密码',
    `reg_from` varchar(20) NOT NULL DEFAULT '' COMMENT '注册来源',
    `create_time` datetime NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`uid`),
    KEY `username` (`username`)
) ENGINE=InnoDB;

-- 全局id表, 避免项目合表时出现主键冲突的情况
CREATE TABLE `global_id` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `create_day` char(8) not null default '' comment '年月日:20211206',
     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- 角色表
CREATE TABLE `role` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(10) NOT NULL DEFAULT '0' COMMENT '角色名',
    `project` varchar(50) NOT NULL DEFAULT '' COMMENT '项目名',
    `access` text NOT NULL COMMENT '该角色可访问的模块和方法列表,json',
    `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态',
    `create_time` datetime NOT NULL DEFAULT current_timestamp(),
    `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `project` (`project`)
) ENGINE=InnoDB AUTO_INCREMENT=1 COMMENT='权限管理的角色表'

-- 角色与用户的绑定关系表,一个用户一个角色
CREATE TABLE `role_bind` (
     `id` int(10) unsigned NOT NULL DEFAULT 0 AUTO_INCREMENT,
     `unid` char(32) NOT NULL DEFAULT '' COMMENT '角色唯一值id',
     `uid` char(32) NOT NULL DEFAULT 0 COMMENT '用户id',
     `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '状态',
     `username` varchar(30) NOT NULL DEFAULT '' COMMENT '用户姓名',
     `roleid` char(32) NOT NULL DEFAULT '' COMMENT '角色id',
     `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB COMMENT='角色与uid的绑定关系';