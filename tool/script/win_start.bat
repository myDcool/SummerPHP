@echo off

rem windows 下启动PHP接收网络请求的进程, 建议配合NGINX使用
rem 先下载php源码, 解压到比如 D:/server/php/ 目录下边, 并更改源码中的配置文件名为 php.ini
rem 然后将此脚本放在 D:/server/ 下边
rem 启动进程前杀掉已有进程
rem taskkill /f /im php-cgi.exe

set currentDir=%cd%

set "phpCGI=%currentDir%\php\php-cgi.exe"
set "phpCLI=%currentDir%\php\php.exe"
set "phpConf=%currentDir%\php\php.ini"

%phpCLI% -v
@echo on

%phpCGI% -b 127.0.0.1:9000 -c %phpConf%

