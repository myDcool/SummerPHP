# CornerPHP
##### 模块化, 静态化, 层级少, 易调试, 支持多数据库链接/读写分离
##### 参考文档: http://doc.hearu.top/
##### 原SummerPHP
##### 若要支持命名空间请参考：https://gitee.com/myDcool/meng-php （推荐，如有bug两个框架会同步更新）

## 目录结构
```
/ Framework Root
|-- core    框架的核心类
|-- config  配置文件
|-- libs    第三方库
|-- model   模型类, 理论上用于写获取数据的具体逻辑, 只放置在根目录下, 任何控制器都可以调用到
|-- modules 项目模块
|-- view   视图文件
|-- tool   框架自带的登录注册模块的SQL，数据库配置文件样例，windows启动php-cgi的脚本
|-- update   框架自更新脚本
|-- static  静态文件存放
|-- cli.php  命令行下的入口文件 php cli.php -q router_name
`-- index.php   web入口文件
```

## 安装步骤
1. 下载框架代码
2. 配置NGINX
    ````
    server {
        listen       80;
        server_name  www.test.com;
    
        location ~ \.ico|jpg|gif|png|js|css$ {
            root E:/php/code/project/static;
            #expires 1h;
        }

        location / {
            root E:/php/code/project;
            fastcgi_pass   127.0.0.1:9720;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $document_root/index.php;
            include        fastcgi_params;
        }
    
    }
    ````
3. 修改hosts文件，添加：
    ````
    127.0.0.1 www.test.com
    
    ````
4. 复制并修改 tool/dbConfig.json 文件中的数据库链接信息, 放到某个目录下; 修改入口文件index.php/cli.php 中的`MYSQL_HOST_FILE`值为dbConfig.json的文件路径
5. 启动nginx; 启动php: php-cgi.exe（windows） 或 php-fpm (Linux)
6. 在浏览器中输入 www.test.com 访问首页 （首页默认是数据库信息）

## 核心功能使用说明
### URL路由
> 参考 core/Route.php (配置文件: config/RouteConfig.php)
1. 配置: 所有对外可访问的接口都要在 config/RouteConfig.php中配置映射规则, 接口与真正的方法是映射关系, 不直接暴露源代码的方法名

2. 例子: 获取文章列表的第二页接口的映射规则为

```
//路由配置:
'article_list_(\d+)' => 'index/index/route/page/$1'
```

```
//浏览器中输入URL:
http://www.test.com/article_list_2 

```  

3. 默认路由: 如果浏览器中只输入域名，没有URI，框架会找到 modules/index/index.php::index() 方法并执行 (也可以在RouteConfig.php中配置默认路由)

4. 命令行路由:
```
php cli.php -q article_list_2
```

### 获取请求数据
> 参考 core/Request.php (数据校验的配置在config/VerifyConfig.php)

1. 获取一个值
```
Request::Get('a', 'default');
Request::Post('a');
Request::Cookie('a');
Request::Route('a');
```

2. 获取多个值, 没有则用默认值替代
```
Request::pickData(['a' => 0, 'b' => '', 'c' => '0'], 'get')
```

3. 是否是post请求
```
Request::isPost();
```

### 返回结果
> 参考 core/Response.php

1. 输出固定格式的json数据
```
$a = ['list' => [1,2,3,4]];

Response::json(10000, '用户列表', $a);

结果: {"code":10000,"msg":"\u7528\u6237\u5217\u8868","data":{"list":[1,2,3,4]}}
```

2. 便捷调用
> Response::json()的简写, 返回结构是一样的

```
//成功返回:

Response::success($a);
Response::success($a, '用户列表');
Response::success($a, '用户列表', 20000);
```

```
//失败返回:

Response::error('参数错误'); 
结果: {"code":"-1","msg":"\u53c2\u6570\u9519\u8bef","data":[]]}
Response::error('参数错误', $a); 
结果: {"code":"-1","msg":"\u53c2\u6570\u9519\u8bef","data":{"list":[1,2,3,4]},"url":""}
Response::error('参数错误', $a, 20001); 
结果: {"code":2001,"msg":"\u53c2\u6570\u9519\u8bef","data":{"list":[1,2,3,4]}}
```

3. 返回任意结构的json数据
```
Response::jsonReturn($a); //{"list":[1,2,3,4]}
```

4. 跳转
```
Response::notify('页面找不到啦~'); //页面找不到啦~
Response::redirect('充值成功, 页面即将跳转', 'http://www.hearu.top', 3);
Response::jump('http://www.hearu.top'); //直接跳转
```

### 返回HTML页面
> 参考: core/View.php

1. 显示单个页面
```
View::display();
```

2. 插入式显示页面(一个HTML页面框架, 里边有占位符:{{xxx}})
```
View::render();

```

### 数据库操作
> 更多增删改查操作请查看 modules/index/index.php::sql() 
```
$rs = Test::link('note')
    ->fields('id,content')
    ->where(['uid' => 1, 'age' => 20])
    ->whereOp('id', '>', 1)
    ->whereIn('status', [0,1,2])
    ->order('id desc')
    ->limit(10)
    ->select()
    ->getAll();

    var_dump($rs, Test::$currentSql);
   
```

### 文件日志
> 参考 libs/FileLog.php <br>
> 日志文件存放的目录在入口文件 index.php 和 cli.php 中配置: LOGPATH 和 LOG_LEVEL
1. 常用
```
FileLog::info([xxx], 'tag');  //日志文件: LOGPATH/yyyy-mm-dd.log, 数组内容会被转为json
或
FileLog::error('xxxx');
```

2. 设置日志跟踪ID
```
FileLog::$uuid = time().Fun::randChar(5);
```

### Redis消息队列
- 基础类在: libs/IRedis.php
- 队列类在: model/RedisQueue.php
- 队列名的配置在: config/RedisConfig.php
- 具体实现参考: modules/cli/queue.php

### DB消息队列
- 队列类在: model/DBQueue.php
- Topic的配置在: config/TopicConfig.php
- 需要添加消费者的crontab (路由: cli_queue_db_consumer), 代码在 module/cli/queue_db.php
 
### 自带登录注册模块
- 用户信息用cookie加密存储
- 功能有: 登录/注册/退出, 其中注册提供图形验证码

### 自带前端单页面应用（SPA）样例
- 按照上边说明安装好PHP运行环境
- 配置好数据库信息（dbConfig.php）
- 浏览器访问首页，即是一个可查看个多主机/数据库/表/字段等信息的web应用

### 自带多个js插件
> 路径: static/sys/js (使用方法在每个js文件的底部, 也可参考 https://exchange.hearu.top/ 一个个人商城项目)

| 脚本名  | 说明  |
|---|---|
| zbActionSheet.js  | 弹窗表单, 支持 单行文本/多行文本/单选/多选/添加图片  |
| zbEditTable.js  | 可编辑表格, 支持 创建可编辑的表格, 只读表格; 支持动态增加/删除行; 支持批量更改属性; 支持批量获取输入的值  |
| zbImage.js  | 图片拼接: 支持创建二维码, 二维码加文字, 图片加二维码水印(依赖 在static/lib/jquery-qrcode.min.js); 支持创建浮层图片(点击后隐藏)  |
| zbUploadImage.js  | 上传图片: 支持跟随上传其他参数, 支持图片压缩(依赖 static/lib/lrz.min.js)  |
| zbList.js  | 列表: 支持onclick事件; 支持仅展示无响应事件的静态列表  |
| zbPanel.js  | 可折叠的列表: 支持点击后回调  |
| zbRequest.js  | 发起http请求: 支持缓存(依赖 static/lib/md5.min.js); 支持自定义成功后回调  |
| zbRouter.js  | 单页面路由: 支持匹配URL调用指定的回调方法, 并无刷新更新浏览器地址栏URL  |
| zbTabNav.js  | 导航栏: 支持点击后tab高亮, 调用回调, 页面无刷新切换, 可放在页面任意位置  |
| zbToolBar.js  | 工具栏: 支持显示在底部或顶部显示  |
| zbTool.js  | 工具方法合集: 获取年月日; 获取随机数; 字符串拼接补全; 获取URL参数; 跳转页面,刷新页面; 字符串和json互转; html转dom; 数组转HTML; 回到顶部; 等  |
 

### 安全建议
- 尽量用post: 绝大部分的xss攻击是通过 <img> <script> <a> <link> 等标签的 src 属性或href属性发起的"get"请求
- 值中不能有\0截断符
- 添加验证码: 防止外部构造表单请求, 要用在关键的地方(登录注册等), 不要影响到用户
- 检测refer: 防止外部构造表单请求, 防止jsonp攻击, csrf攻击
- 检测refer: 在返回头中构造白名单, header('Access-Control-Allow-Origin: http://www.a.com')
- 一次性token: 用完就释放, 防止表单重复提交, 防止csrf攻击
- include, file_get_content等使用时如果参数是URL,就得验证是否是白名单内的域名, 否则会包含/引入危险的文件
- 对参数进行url_decode 防止 相对路径攻击(../../....)和javascript变相攻击
- set cookie 的时候最好设置httponly, 这样就只能通过抓包来获取cookie注入脚本的方法就不行了
- 获取cookie时限制跟ip相关, 这样通过xss的方法获取的cookie就不能被乱用了
- html<base href=""> 标签可以指定本页面以后相对路径的根路径URL, 所以链接都要用全URL路径
- 表单中过滤掉html注释符 "<!--" "-->", 防止表单回填的时候被攻击
- 表单中过滤掉select insert replace delete
- 上传的临时文件夹不要有可执行的权限，最好用第三方存储