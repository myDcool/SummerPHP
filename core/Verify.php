<?php
defined('ENV') || exit('illegal Access! @110');
/**
 * 验证类，验证传入参数的合法性
 * 配合config/verifyConfing.php 使用
**/
class Verify
{
    public static $message='';
    const ASCII_0 = 48;
    const ASCII_9 = 57;
    
    const ASCII_A = 65;
    const ASCII_Z = 90;
    
    const ASCII_a = 97;
    const ASCII_z = 122;

    const rule_empty = '/^.{0}$/'; //空字符串
    const rule_alpha = '/[a-zA-Z]+/'; //英文字母
    const rule_alpha_lower = '/[a-z]+/'; //小写英文字母
    const rule_alpha_upper = '/[A-Z]+/'; //大写英文字母
    const rule_number = '/[0-9]+/'; //数字
    const rule_alpha_number = '/[a-zA-Z0-9]+/'; //英文字母,数字
    const rule_required = '/.+/'; //长度不为0
    const rule_cn = '/[\x{4e00}-\x{9fa5}]+/u'; //汉字
    const rule_alpha_number_cn = '/[a-zA-Z0-9\x{4e00}-\x{9fa5}]+/u'; //字母,数字,汉字

    /**
     * 验证参数是否合法
     * @param $paramKey
     * @param $paramValue
     * @param string $k 配置文件中的键名
     * @return bool
     * @throws Exception
     */
    public static function check($paramKey, $paramValue, $k='index:index:index')
    {
        $config = VerifyConfig::getConfig();
        if (!empty($config[$k][$paramKey])) {
            $verify = $config[$k][$paramKey];

            $func = $verify['rule']; //校验数据的方法名
            $rulers = $verify['ruler']; //校验规则


            if (method_exists(__CLASS__,$func)) {
                $return = Verify::$func($paramValue, $rulers);
            } else {
                self::$message = '校验方法不存在: '.$func;
                return false;
            }

            if (!$return) {
                self::$message = $verify['msg'];
                return false;
            }
        }
    
        return TRUE;
    }

    //匹配所有的条件
    public static function allOf($str, $rules)
    {
        foreach ($rules as $p) {
            if (preg_match($p, $str) === 0) {
                return false;
            }
        }

        return true;
    }

    //匹配任一条件
    public static function anyOf($str, $rules)
    {
        foreach ($rules as $p) {
            if (preg_match($p, $str) === 1) {
                return true;
            }
        }

        return false;
    }

    //只匹配其中一个条件, 互斥
    public static function oneOf($str, $rules)
    {
        $ok = 0;
        foreach ($rules as $p) {
            if (preg_match($p, $str) === 1) {
                $ok += 1;
            }
        }

        return ($ok == 1);
    }

    //不满足条件
    public static function not($str, $rules)
    {
        foreach ($rules as $p) {
            if (preg_match($p, $str) === 1) {
                return false;
            }
        }

        return true;
    }

    //一定范围的长度
    public static function getRuleLengthRange($min,$max='')
    {
        if (!empty($min) && !empty($max)) {
            return '/^.{'.$min.','.$max.'}$/u';
        } else if (!empty($min)) {
            return '/^.{'.$min.',}$/u';
        } else {
            return '/^.{,'.$max.'}$/u';
        }
    }

    //固定长度
    public static function getRuleLength($len)
    {
        return '/^.{'.$len.'}$/u';
    }

    //手机号码
    public static function getRuleMobile($country='zh_cn')
    {
        switch ($country) {
            case 'zh_cn':
                $rule = '/^1[3-9]{10}$/';
                break;
            default:
                $rule = '/^1[3-9]{10}$/';
                break;
        }
        
        return $rule;
    }

    public static function getRuleEmail()
    {
        return '/.+@.+\..+/';
    }

    //检查是否是合法邮箱
    public static function isEmail($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $arr = explode("@",$value);
            $domain = array_pop($arr);
            return checkdnsrr($domain,"MX");
        } else {
            return FALSE;
        }
    }
	
}

