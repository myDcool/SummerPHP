<?php

class _consumer extends base
{
    public function handle()
    {
        FileLog::info($_POST, '处理异步消息');

        $msgCode = Request::Post('msg_code');
        $topic = Request::Post('topic_name');

        $handleId = DBQueue::addHandleRecord(PROJECT_NAME, $msgCode, $topic);
        switch ($topic) {
            case TopicConfig::topic_user_register:
                $userCode = Request::Post('user_code');

                break;
            case TopicConfig::topic_user_login:
                $userCode = Request::Post('user_code');
                FileLog::info("用户登录. $userCode ".REQUEST_DATETIME);
                break;
            default:
                FileLog::info("$topic $msgCode 未监听, 不处理");
        }
        DBQueue::updateHandleStatus($handleId, DBQueue::handle_status_end);
        Response::success(['topic'=>$topic, 'msg_code'=>$msgCode, 'handle_id'=>$handleId]);
    }
}
