<?php

class _record extends base
{
    public function test()
    {
        $msgCode = Request::Post('msg_code');
        $topic = Request::Post('topic_name');
        $handleId = DBQueue::addHandleRecord(PROJECT_ID, $msgCode, $topic);
        FileLog::info($_POST, __METHOD__);
        DBQueue::updateHandleStatus($handleId, DBQueue::handle_status_end);
    }

    public function add()
    {
        FileLog::info($_POST, __METHOD__);
    }

    public function modify()
    {
        FileLog::info($_POST, __METHOD__);
    }

    public function delete()
    {
        FileLog::info($_POST, __METHOD__);
    }
}
