<?php

class base
{
    public function __construct()
    {
        $this->setTraceId();
        $this->checkSign();
        $this->checkRepeat();
    }

    //设置日志跟踪ID
    public function setTraceId()
    {
        FileLog::$uuid = Request::Post('trace_id', UNIQID);
    }

    //校验签名
    public function checkSign()
    {
        $appId = Request::Post('app_id');
        $isOk = Sign::compareMd5Sign($appId, $_POST);
        if ($isOk === false) {
            FileLog::info($_POST, '验签错误. '.Sign::$error);
            Response::error(Sign::$error);
        } else {
            return true;
        }
    }

    //校验消息是否重复
    public function checkRepeat()
    {
        $msgCode = Request::Post('msg_code');
        $rs = DBQueue::hasHandled(PROJECT_NAME, $msgCode);
        if (!empty($rs)) {
            FileLog::info($_POST, '消息重复');
            Response::error("已消费过此消息, 不再重复消费. ", [PROJECT_NAME, $msgCode]);
        } else {
            return true;
        }
    }
}