<?php

/**
 * Class _queue
 * 队列操作, 基础类, 勿删
 */
class _queue
{
    public $queueKey = '';

    /**
     * 写入队列
     * 队列名统一都在RedisConfig类中维护
     * @param string $queueName 队列名
     * @param string  $message json消息体
     */
    public function push($queueName, $message)
    {
        return RedisQueue::pushQueue($queueName, $message);
    }
    
    /**
     * 阻塞出队列, 处理某个队列
     * 配合crontab 或 supervisor等进程监控程序使用 * * * * * cd path/to/code && php cli.php -q queue_blockpop_(queue_name)
     * 队列名统一都在RedisConfig类中维护
     */
    public function blockpop()
    {
        if (Fun::isProcessRun(Route::$path)) {
            echo Fun::getDateTime().' 进程已经在运行. '.Route::$path.PHP_EOL;
            return true;
        }

        echo Fun::getDateTime().' 启动进程. '.Route::$path.PHP_EOL;

        $queueKey = Request::Route('queueKey');
        RedisQueue::blockPopQueue($queueKey);
    }

    /**
     * 阻塞出队列, 处理配置文件中的所有read队列
     * 配合crontab 或 supervisor等进程监控程序使用 * * * * * cd path/to/code && php cli.php -q queue_brpop_all
     * 队列名统一都在RedisConfig类中维护
     */
    public function blockpopAll()
    {
        if (Fun::isProcessRun(Route::$path)) {
            echo Fun::getDateTime().' 进程已经在运行. '.Route::$path.PHP_EOL;
            return true;
        }

        echo Fun::getDateTime().' 启动进程. '.Route::$path.PHP_EOL;

        $queueNameList = array_keys(RedisConfig::$notifyUrl);
        RedisQueue::blockPopQueue($queueNameList, 50);
    }
    
    /**
     * 退出pop进程
     */
    public function kill()
    {
        $queueKey = Request::Route('queueKey');
        RedisQueue::killPopQueue($queueKey);
    }

    /**
     * 重启pop进程
     * 在部署了新代码的时候用到
     * @param string $queueKey
     */
    public function restart()
    {
        $queueKey = Request::Route('queueKey');
        RedisQueue::restartQueue($queueKey);
    }

    /**
     * 测试 redis消息队列, 生产一条测试消息
     */
    public function test()
    {
        $rs = RedisQueue::pushQueue(RedisConfig::topic_test, ['a'=>11111, 'b' => '11111']);
        var_dump($rs);
    }
}