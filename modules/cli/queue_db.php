<?php

class _queue_db
{
    public function initc()
    {
        if (Fun::isProcessRun('queue_db_consumer')) { //检查消费者进程是否存在, 此名字在RouteConfig.php中配置
            //echo date('Y-m-d H:i:s'). '已存在进程 queue_db_consumer'.PHP_EOL;
            exit; //已有进程在跑, 本次停止
        } else {
            echo date('Y-m-d H:i:s'). '启动进程 queue_db_consumer '.PHP_EOL;
        }
    }

    public function consumer()
    {
        $minute = date('YmdHi'); //当前分钟数
        while(true) {
            $minute_now = date('YmdHi');
            if ($minute != $minute_now) {
                echo date('Y-m-d H:i:s'). '跨分钟, 停止本次执行.'.PHP_EOL;
                exit; //跨分钟退出
            }

            $row = DBQueue::pop(); //取出最近一条
            if (empty($row)) {
                sleep(5); //没有要处理的数据, 挂起5秒钟
            } else {
                echo '开始处理消息: '.$row['id'].PHP_EOL;
                FileLog::$uuid = time().Fun::randChar(5); //设置日志的跟踪ID
                DBQueue::handle($row);
            }
        }
    }

}