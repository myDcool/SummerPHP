<?php

class _index
{
    public function index()
    {
        View::display('index');
    }

    //生成图片验证码
    public function vcode()
    {
        $width = Request::Get('w', 80);
        $height = Request::Get('h', 40);
        $code=new ValidationCode($width, $height, 4);
        $code->showImage();
        $_SESSION['reg_vcode'] = $code->getCheckCode();
        $_SESSION['reg_vcode_lasttime'] = REQUEST_TIME;
        FileLog::info($_SESSION['reg_vcode'], 'user');
    }

    //省份列表
    public function province()
    {
        $country = Request::Post('country');
        $list = Model::link('area')
            ->fields('province_code, province')
            ->where(['status' => 0, 'country_code' => $country])
            ->select()
            ->getAll();
        $list = array_column($list, 'province', 'province_code');
        Response::success($list);
    }

    //城市列表
    public function city()
    {
        $province = Request::Post('province');
        $list = Model::link('area')
            ->fields('city_code, city')
            ->where(['status' => 0, 'province_code' => $province])
            ->select()
            ->getAll();
        $list = array_column($list, 'city', 'city_code');
        Response::success($list);
    }

    //县区列表
    public function area()
    {
        $city = Request::Post('city');
        $list = Model::link('area')
            ->fields('area_code, area')
            ->where(['status' => 0, 'city_code' => $city])
            ->select()
            ->getAll();
        $list = array_column($list, 'area', 'area_code');
        Response::success($list);
    }
}
