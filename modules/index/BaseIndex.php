<?php

/**
 * desc 前台用户访问基类
 * Class BaseIndex
 */
class BaseIndex
{
	protected $uid = false;
	protected $userinfo = [];
	protected $notNull = []; //不能为空的数据

	public function __construct()
	{
		$user = User::getUserCookie(User::$UserCookieName);
		if (!empty($user)) {
            $this->uid = empty($user['unid']) ? '' : $user['unid'];
			$this->userinfo = $user;
			User::setUserCookie($user); //重新计算有效期
		}
        
        if (empty($this->uid)) {
            //Response::error('您还未登录');
            Response::jump(Fun::buildUrl('user_login', ['from' => REQUEST_URI]));
        }
	}
}